﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceGame : MonoBehaviour {

    private Text tempoDecorrido;
    private int segundos = 0, minutos = 0;

	// Use this for initialization
	void Start () {
        tempoDecorrido = GameObject.Find("Tempo").GetComponent<Text>();
        StartCoroutine(ContadorSegundos());
    }

    public IEnumerator ContadorSegundos()
    {
        yield return new WaitForSeconds(1);
        segundos += 1;
        tempoDecorrido.text = "Tempo: " + segundos;
        StartCoroutine(ContadorSegundos());
    }
}
